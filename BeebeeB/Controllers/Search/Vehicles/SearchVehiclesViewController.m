//
//  SearchVehiclesViewController.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/14/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "SearchVehiclesViewController.h"
#import "Constants.h"

@interface SearchVehiclesViewController ()

@end

@implementation SearchVehiclesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(([NSString stringWithFormat:@"%@%d", kMenuItemTitle, 0]), nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
