//
//  ForgotPassViewController.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/15/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "ForgotPassViewController.h"
#import "BeBTextField.h"
#import "Constants.h"

@interface ForgotPassViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblForgotPass;
@property (weak, nonatomic) IBOutlet BeBTextField *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSendPass;

@end

@implementation ForgotPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self localizeItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)localizeItems {
    
    _lblForgotPass.text = NSLocalizedString(kFORGOTPLCForgotPass, nil);
    _txtEmail.placeholder = NSLocalizedString(kPLCEmail, nil);
    
    [_btnCancel setTitle:NSLocalizedString(kFORGOTPLCCancel, nil) forState:UIControlStateNormal];
    [_btnSendPass setTitle:NSLocalizedString(kFORGOTPLCSendPass, nil) forState:UIControlStateNormal];
}

- (IBAction)dissmissView:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
