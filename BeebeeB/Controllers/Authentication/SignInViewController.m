//
//  SignInViewController.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/14/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "SignInViewController.h"
#import "BeBTextField.h"
#import "BeBButton.h"
#import "Constants.h"

@interface SignInViewController ()

@property (weak, nonatomic) IBOutlet BeBTextField *txtUsername;
@property (weak, nonatomic) IBOutlet BeBTextField *txtPassword;
@property (weak, nonatomic) IBOutlet BeBButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPass;
@property (weak, nonatomic) IBOutlet UILabel *lblOr;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(kVCTitleSignIn, nil);
    [self localizeItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)localizeItems {
    
    _txtUsername.placeholder = NSLocalizedString(kPLCUserName, nil);
    _txtPassword.placeholder = NSLocalizedString(kPLCPassword, nil);
    _lblOr.text = NSLocalizedString(kSIGNINPLCOr, nil);
    
    [_btnLogin setTitle:NSLocalizedString(kSIGNINPLCLogin, nil) forState:UIControlStateNormal];
    [_btnForgotPass setTitle:NSLocalizedString(kSIGNINPLCForgot, nil) forState:UIControlStateNormal];
    [_btnSignUp setTitle:NSLocalizedString(kSIGNINPLCSignup, nil) forState:UIControlStateNormal];
}

- (IBAction)login:(id)sender {
}

- (IBAction)forgotPassword:(id)sender {
}

@end
