//
//  SignUpViewController.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/14/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "SignUpViewController.h"
#import "BeBTextField.h"
#import "BeBButton.h"
#import "Constants.h"
#import "DatePickerViewController.h"
#import "Utilities.h"

@interface SignUpViewController () <DatePickerDelegate, UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) IBOutlet BeBTextField *txtName;
@property (weak, nonatomic) IBOutlet BeBTextField *txtPrimaryPhone;
@property (weak, nonatomic) IBOutlet BeBTextField *txtSecondaryPhone;
@property (weak, nonatomic) IBOutlet BeBTextField *txtLandlinePhone;
@property (weak, nonatomic) IBOutlet BeBTextField *txtLicenseEndDate;
@property (weak, nonatomic) IBOutlet BeBTextField *txtWorkFor;
@property (weak, nonatomic) IBOutlet BeBTextField *txtJobTitle;
@property (weak, nonatomic) IBOutlet BeBTextField *txtLoginName;
@property (weak, nonatomic) IBOutlet BeBTextField *txtPassword;
@property (weak, nonatomic) IBOutlet BeBTextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet BeBTextField *txtEmail;
@property (weak, nonatomic) IBOutlet BeBTextField *txtSecretQuestion;
@property (weak, nonatomic) IBOutlet BeBTextField *txtSecretAnswer;
@property (weak, nonatomic) IBOutlet BeBButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIButton *btnPickDate;

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(kVCTitleSignUp, nil);
    
    [self localizeItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)localizeItems {
    
    _txtName.placeholder = NSLocalizedString(kSIGNUPPLCName, nil);
    _txtPrimaryPhone.placeholder = NSLocalizedString(kSIGNUPPLCPrimaryPhone, nil);
    _txtSecondaryPhone.placeholder = NSLocalizedString(kSIGNUPPLCSecondaryPhone, nil);
    _txtLandlinePhone.placeholder = NSLocalizedString(kSIGNUPPLCLandlinePhone, nil);
    _txtLicenseEndDate.placeholder = NSLocalizedString(kSIGNUPPLCLicenseEndDate, nil);
    _txtWorkFor.placeholder = NSLocalizedString(kSIGNUPPLCWorkFor, nil);
    _txtJobTitle.placeholder = NSLocalizedString(kSIGNUPPLCJobTitle, nil);
    _txtLoginName.placeholder = NSLocalizedString(kPLCUserName, nil);
    _txtPassword.placeholder = NSLocalizedString(kPLCPassword, nil);
    _txtConfirmPassword.placeholder = NSLocalizedString(kSIGNUPPLCConfirmPassword, nil);
    _txtEmail.placeholder = NSLocalizedString(kPLCEmail, nil);
    _txtSecretQuestion.placeholder = NSLocalizedString(kSIGNUPPLCSecretQuestion, nil);
    _txtSecretAnswer.placeholder = NSLocalizedString(kSIGNUPPLCSecretAnswer, nil);
    
    [_btnSignUp setTitle:NSLocalizedString(kVCTitleSignUp, nil) forState:UIControlStateNormal];
}

- (IBAction)presentDatePopover:(id)sender {
    
    DatePickerViewController* popoverContent = [[DatePickerViewController alloc] initWithNibName:kXIBDatePicker bundle:nil];
    
    popoverContent.modalPresentationStyle = UIModalPresentationPopover;
    popoverContent.preferredContentSize = CGSizeMake(300, 200);
    popoverContent.delegate = self;
    if (![_txtLicenseEndDate.text isEqualToString:@""]) {
        popoverContent.date = [Utilities dateFromDateString:_txtLicenseEndDate.text];
    }
    else {
        popoverContent.date = [NSDate date];
    }
    
    UIPopoverPresentationController *popover = popoverContent.popoverPresentationController;
    popover.delegate = self;
    popover.sourceView = sender;
    popover.sourceRect = _btnPickDate.bounds;
    popover.permittedArrowDirections = UIPopoverArrowDirectionDown;
    
    [self presentViewController:popoverContent animated:YES completion:nil];
}

- (IBAction)signUp:(id)sender {
}

- (void)dateChenged:(NSDate *)date {
    
    _txtLicenseEndDate.text = [Utilities dateStringFromDate:date];
}

@end
