//
//  DatePickerViewController.h
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/15/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DatePickerDelegate <NSObject>

- (void)dateChenged:(NSDate*)date;

@end

@interface DatePickerViewController : UIViewController

@property id<DatePickerDelegate> delegate;

@property NSDate *date;

@end
