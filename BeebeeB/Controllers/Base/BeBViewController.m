//
//  BeBViewController.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/13/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "BeBViewController.h"
#import "CDRTranslucentSideBar.h"
#import "MenuCell.h"
#import "Constants.h"
#import "SearchVehiclesViewController.h"

@interface BeBViewController () <CDRTranslucentSideBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) CDRTranslucentSideBar *sideBar;

@end

@implementation BeBViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initNavigationBar];
    [self initSideMenu];
    [self hideAllButCurrentViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller{
    return UIModalPresentationNone;
}

- (void)initNavigationBar {
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:kPNGMenu]
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(showMenu)];
    [self initNavigationHomeTitle];
}

- (void)showMenu {
    
    [self.sideBar show];
}

- (void)initNavigationHomeTitle {
    
    UIView *homeButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    homeButtonContainer.backgroundColor = [UIColor clearColor];
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setFrame:CGRectMake(0, 0, 80, 44)];
    [homeButton setBackgroundImage:[UIImage imageNamed:kPNGLogo] forState:UIControlStateNormal];
    [homeButton addTarget:self action:@selector(navigateToHome) forControlEvents:UIControlEventTouchUpInside];
    [homeButton setShowsTouchWhenHighlighted:YES];
    [homeButtonContainer addSubview:homeButton];
    self.navigationItem.titleView = homeButtonContainer;
}

- (void)navigateToHome {
    
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
}

- (void)initSideMenu {
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kEN]) {
        self.sideBar = [[CDRTranslucentSideBar alloc] init];
    }
    else if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kAR]) {
        self.sideBar = [[CDRTranslucentSideBar alloc] initWithDirectionFromRight:YES];
    }
    self.sideBar.delegate = self;
    
    [self setupSideMenu];
}

- (void)setupSideMenu {
    
    UITableView *tableView = [[UITableView alloc] init];
    [tableView registerNib:[UINib nibWithNibName:kCellIDMenu bundle:nil] forCellReuseIdentifier:kCellIDMenu];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor whiteColor];
    //tableView.scrollEnabled = NO;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, self.navigationController.navigationBar.frame.size.height + 20)];
    [self customizeHeaderView:headerView];
    tableView.tableHeaderView = headerView;
    
    [self.sideBar setContentViewInSideBar:tableView];
    [self.sideBar setTranslucentTintColor:[UIColor whiteColor]];
}

- (void)customizeHeaderView:(UIView *)headerView {
    
    headerView.backgroundColor = [UIColor orangeColor];
    headerView.alpha = 0.5;
    
    UILabel *lblWelcome = [[UILabel alloc] initWithFrame:CGRectMake(20, 40, 50, 20)];
    [lblWelcome setFont:[UIFont systemFontOfSize:12]];
    lblWelcome.text = NSLocalizedString(kMenuHeaderWelcome, nil);;
    lblWelcome.textColor = [UIColor whiteColor];
    [lblWelcome sizeToFit];
    [headerView addSubview:lblWelcome];
    
    UIButton *signInButton = [[UIButton alloc] initWithFrame:CGRectMake(lblWelcome.frame.size.width, 28, 80, 40)];
    signInButton.backgroundColor = [UIColor clearColor];
    [signInButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
    [signInButton setTitle:NSLocalizedString(kVCTitleSignIn, nil) forState:UIControlStateNormal];
    [signInButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signInButton addTarget:self
                     action:@selector(pushAuthenticationStoryboard)
           forControlEvents:UIControlEventTouchUpInside];
    
    if ([[self.storyboard valueForKey:@"name"] isEqualToString:kSBNameAuthentication]) {
        signInButton.userInteractionEnabled = NO;
    }
    [headerView addSubview:signInButton];
    
    lblWelcome.translatesAutoresizingMaskIntoConstraints = NO;
    signInButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *yLblCenterConstraint = [NSLayoutConstraint constraintWithItem:lblWelcome
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:headerView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0
                                                                             constant:0];
    [headerView addConstraint:yLblCenterConstraint];
    
    NSLayoutConstraint *yBtnCenterConstraint = [NSLayoutConstraint constraintWithItem:signInButton
                                                                            attribute:NSLayoutAttributeCenterY
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:headerView
                                                                            attribute:NSLayoutAttributeCenterY
                                                                           multiplier:1.0
                                                                             constant:0];
    [headerView addConstraint:yBtnCenterConstraint];
    
    NSLayoutConstraint *leadingLblConstraint = [NSLayoutConstraint constraintWithItem:lblWelcome
                                                                            attribute:NSLayoutAttributeLeading
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:headerView
                                                                            attribute:NSLayoutAttributeLeading
                                                                           multiplier:1.0
                                                                             constant:20];
    [headerView addConstraint:leadingLblConstraint];
    
    NSLayoutConstraint *leadingBtnConstraint = [NSLayoutConstraint constraintWithItem:signInButton
                                                                            attribute:NSLayoutAttributeLeading
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:lblWelcome
                                                                            attribute:NSLayoutAttributeTrailing
                                                                           multiplier:1.0
                                                                             constant:8];
    [headerView addConstraint:leadingBtnConstraint];
}

- (void)hideAllButCurrentViewController {
    
    for (int index = 0; index < self.navigationController.viewControllers.count; index++) {
        BeBViewController *vc = [self.navigationController.viewControllers objectAtIndex:index];
        vc.view.alpha = 0.0;
    }
    
    self.view.alpha = 1.0;
    self.sideBar.view.alpha = 1.0;
}

- (void)pushAuthenticationStoryboard {
    [self pushStoryboard:kSBNameAuthentication];
}

#pragma mark - Table view
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(section == 0 )
        return 0;
    
    return 1;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
    headerView.backgroundColor = [UIColor blackColor];
    
    return headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 4;
            break;
        case 2:
            return 3;
            break;
        case 3:
            return 8;
            break;
        default:
            return 17;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger generalCount = indexPath.row;
    for (int previousSections = 0; previousSections < indexPath.section; previousSections++) {
        generalCount += [tableView numberOfRowsInSection:previousSections];
    }
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIDMenu forIndexPath:indexPath];
    NSString *itemTitleKey = [NSString stringWithFormat:@"%@%ld", kMenuItemTitle, (long)generalCount];
    cell.title.text = NSLocalizedString(itemTitleKey, nil);
    cell.imgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%ld", kMenuItemImage, (long)generalCount]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.sideBar dismissAnimated:YES];
    
    NSInteger generalCount = indexPath.row;
    for (int previousSections = 0; previousSections < indexPath.section; previousSections++) {
        generalCount += [tableView numberOfRowsInSection:previousSections];
    }
    
    switch (generalCount) {
        case 0: {
            [self pushStoryboard:kSBNameSearchVehicles];
            break;
        }
        default:
            break;
    }
}

#pragma mark = CDR Delegate
- (void)sideBar:(CDRTranslucentSideBar *)sideBar willAppear:(BOOL)animated {
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.view.alpha = 0.5;
    }];
}

- (void)sideBar:(CDRTranslucentSideBar *)sideBar willDisappear:(BOOL)animated {
    
    [UIView animateWithDuration:0.3 animations:^(void) {
        self.view.alpha = 1.0;
    }];
}

#pragma mark - Helper methods
- (void)pushStoryboard:(NSString *)sbName {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:sbName bundle: nil];
    BeBViewController *viewController = [storyboard instantiateInitialViewController];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)pushVC:(NSString *)vcName {
    
    BeBViewController *viewController = [self.storyboard instantiateInitialViewController];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
