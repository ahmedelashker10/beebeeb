//
//  MenuCell.h
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/13/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *title;

@end
