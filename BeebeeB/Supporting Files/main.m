//
//  main.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/13/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
