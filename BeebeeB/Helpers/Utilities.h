//
//  Utilities.h
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/15/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject

+ (NSDate*)dateFromDateString:(NSString*)dateString;

+ (NSString*)dateStringFromDate:(NSDate*)date;

@end
