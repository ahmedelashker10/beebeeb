//
//  Utilities.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/15/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (NSString*)dateStringFromDate:(NSDate*)date {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate*)dateFromDateString:(NSString*)dateString {
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    return [dateFormatter dateFromString:dateString];
}

+ (BOOL)validateName:(NSString *)string {
    
    NSError *error             = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:
                                  @"[a-zA-Z ]" options:0 error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
    
    return numberOfMatches == string.length;
}

+ (BOOL)validateEmail:(NSString *)checkString {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
