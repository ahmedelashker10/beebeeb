//
//  Constants.h
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/13/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

// LANGUAGES
static NSString * const kAR = @"ar-US";
static NSString * const kEN = @"en-US";

// IMAGES
static NSString * const kPNGMenu = @"menu.png";
static NSString * const kPNGLogo = @"logo.png";

// XIBs
static NSString * const kXIBDatePicker = @"DatePickerViewController";

// MENU
static NSString * const kMenuHeaderWelcome = @"kMenuHeaderWelcome";
static NSString * const kMenuItemTitle = @"kMenuItemTitle";
static NSString * const kMenuItemImage = @"menuItemImage";

// STORYBOARDS
static NSString * const kSBNameSearchVehicles = @"SearchVehicles";
static NSString * const kSBNameAuthentication = @"Authentication";

// VIEW CONTROLLERS TITLES
static NSString * const kVCTitleSignIn = @"kVCTitleSignIn";
static NSString * const kVCTitleSignUp = @"kVCTitleSignUp";

// CELL REUSE IDENTIFIERS
static NSString * const kCellIDMenu = @"MenuCell";

// GENERAL PLACEHOLDERS
static NSString * const kPLCEmail = @"kPLCEmail";
static NSString * const kPLCPassword = @"kPLCPassword";
static NSString * const kPLCUserName = @"kPLCUserName";

// SIGN IN VIEW CONTROLLER
static NSString * const kSIGNINPLCPassword = @"kSIGNINPLCPassword";
static NSString * const kSIGNINPLCOr = @"kSIGNINPLCOr";
static NSString * const kSIGNINPLCLogin = @"kSIGNINPLCLogin";
static NSString * const kSIGNINPLCForgot = @"kSIGNINPLCForgot";
static NSString * const kSIGNINPLCSignup = @"kSIGNINPLCSignup";

// FORGOT PASS VIEW CONTROLLER
static NSString * const kFORGOTPLCForgotPass = @"kFORGOTPLCForgotPass";
static NSString * const kFORGOTPLCCancel = @"kFORGOTPLCCancel";
static NSString * const kFORGOTPLCSendPass = @"kFORGOTPLCSendPass";

// SIGN UP VIEW CONTROLLER
static NSString * const kSIGNUPPLCName = @"kSIGNUPPLCName";
static NSString * const kSIGNUPPLCPrimaryPhone = @"kSIGNUPPLCPrimaryPhone";
static NSString * const kSIGNUPPLCSecondaryPhone = @"kSIGNUPPLCSecondaryPhone";
static NSString * const kSIGNUPPLCLandlinePhone = @"kSIGNUPPLCLandlinePhone";
static NSString * const kSIGNUPPLCLicenseEndDate = @"kSIGNUPPLCLicenseEndDate";
static NSString * const kSIGNUPPLCWorkFor = @"kSIGNUPPLCWorkFor";
static NSString * const kSIGNUPPLCJobTitle = @"kSIGNUPPLCJobTitle";
static NSString * const kSIGNUPPLCConfirmPassword = @"kSIGNUPPLCConfirmPassword";
static NSString * const kSIGNUPPLCSecretQuestion = @"kSIGNUPPLCSecretQuestion";
static NSString * const kSIGNUPPLCSecretAnswer = @"kSIGNUPPLCSecretAnswer";
