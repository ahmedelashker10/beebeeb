//
//  BeBTextField.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/14/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "BeBTextField.h"
#import "Constants.h"

@interface BeBTextField () <UITextFieldDelegate>

@end

@implementation BeBTextField

- (void)awakeFromNib {
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kAR]) {
        self.textAlignment = NSTextAlignmentRight;
    }
    
    self.borderStyle = UITextBorderStyleNone;
    [self setBottomBorderColor:[UIColor blackColor]];
    self.delegate = self;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self setBottomBorderColor:[UIColor orangeColor]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self setBottomBorderColor:[UIColor blackColor]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self resignFirstResponder];
    return YES;
}

- (void)setBottomBorderColor:(UIColor *)color {
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height - 1, self.superview.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = color.CGColor;
    [self.layer addSublayer:bottomBorder];
}

@end
