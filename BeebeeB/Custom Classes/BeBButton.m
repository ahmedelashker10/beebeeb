//
//  BeBButton.m
//  BeebeeB
//
//  Created by Ahmed Elashker on 8/14/16.
//  Copyright © 2016 AFATechnologies. All rights reserved.
//

#import "BeBButton.h"

@implementation BeBButton

- (void)awakeFromNib {
    
    self.backgroundColor = [UIColor orangeColor];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

@end
